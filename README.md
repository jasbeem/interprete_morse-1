# README #

### Sobre este repositorio ###

* almacena el código para un microcontrolador de tipo Arduino
* v1.0

### Sobre cómo utilizar este repositorio ###

* el código C en [controller.c](https://bitbucket.org/admin-toolbox-academy/interprete_morse/src/master/controller.c) debe copiarse en la sección "Code" de la página de [diseño](https://www.tinkercad.com/login?next=%2Fthings%2Fh1gYSPuZcjB-dazzling-lappi-bombul%2Feditel%3Fsharecode%3DhWkfjgqWbpl3EQ0s-WY1zrjaVCG7IkNbh230nyG4CtQ)

### Instrucciones de uso ###

* especificaciones del código:  
    1. el controlador debe reconocer una secuencia de 4 pulsos (corto/largo)  
    2. los pulsos se introducen presionando sobre el botón  
    3. si la secuencia es correcta debe encender un led  
    4. si la secuencia es incorreta no debe hacer nada  
* utilizar [camelCase](https://es.wikipedia.org/wiki/Camel_case) para nombrar variables
* utilizar nombre en mayúscula para las constantes (e.g. LED_BUILTIN, OUTPUT)
* puedes editar el código en cualquier momento, dejando claro lo que has cambiado al hacer commit

### Para aprender más ###

* sobre [lenguaje C](https://www.w3schools.com/c/index.php)
* sobre [markdown](https://www.markdownguide.org/cheat-sheet) para contribuir a este fichero README
